# Understanding Turnstile

Turnstile is a system for developing typed DSLs using macros in [Racket](https://racket-lang.org), and was introduced in the paper *Type Systems as Macros*. [1] It's a fascinating idea, but the paper is a little difficult to follow. This repository implements the basics of a type system with macros using straight-forward Racket, and takes up only 68 lines in a single file. (`stlc.rkt`)

I have a write-up about this [on my blog](https://lambdaland.org/posts/2023-08-14_types_with_macros/), which I recommend reading if you're new to this.

## License

MIT.

## Authors

 - [Ashton Wiersdorf](https://lambdaland.org/) (just this particular implementation, not the paper from which the original idea came)

## References

- [1] Chang, S., Knauth, A. and Greenman, B. 2017. <a href="https://doi.org/10.1145/3009837.3009886">Type systems as macros</a>. <i>Proceedings of the 44th ACM SIGPLAN Symposium on Principles of Programming Languages - POPL 2017</i> (Paris, France, 2017), 694–705.
